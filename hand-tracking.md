---
title:"SLAM and Hand Tracking from Scratch"
layout:main
---

# Pre-requisites
This guide is currently mainly for X-Server running on Arch Linux or Arch based Distros and the Rift S and Lenovo Explorer, we will add more at some later point.

__Important__ If you have already installed a version of monado you WILL have to uninstall it before continuing with this guide, in case you are using the AUR or a helper remove the built copy from your AUR directory or the helpers cache.

For other distributions, you will have to refer to the [Monado Basalt](https://gitlab.freedesktop.org/mateosss/basalt/-/tree/xrtslam) repository and Microsofts [ONNX Runtime](https://github.com/microsoft/onnxruntime) repository.

The models required for hand tracking currently need to be pulled via the [get-ht-models.sh](https://gitlab.freedesktop.org/monado/monado/-/blob/main/scripts/get-ht-models.sh) script, otherwise monado-service will crash when built with hand tracking.

## What is all this software?

### Basalt SLAM
Basalt is a 3rd party SLAM(Simultaneous localization and mapping). It is used to enable 6Dof in a multitude of HMDs, like the Rift S or Lenovo Explorer. https://vision.in.tum.de/research/vslam/basalt

### ONNX Runtime
ONNX Runtime is an open source project made by Microsoft that is designed to accelerate machine learning across a wide range of frameworks, operating systems, and hardware platforms. This is whats doing the actual Hand tracking. https://onnxruntime.ai/about.html

### Monado

Monado is an open source XR runtime for the OpenXR API made by Khronos. This is what communicates with your HMD. https://monado.freedesktop.org/

# Setting everything up

## Basalt SLAM
To use Basalt simply clone [basalt-monado-git](https://aur.archlinux.org/packages/basalt-monado-git) and build it.

`git clone https://aur.archlinux.org/basalt-monado-git.git && cd basalt-monado-git && makepkg -si`

Or use your favorite AUR Helper like [yay](https://aur.archlinux.org/yay.git). If you do not want to use the AUR or aren't using an Arch distribution, you can build it from source using [Monado Basalt](https://gitlab.freedesktop.org/mateosss/basalt/-/tree/xrtslam).

If it doesn't work you are probably missing [libuvc-git](https://aur.archlinux.org/libuvc-git.git) and if using a Intel Realsense Camera install [librealsense-git](https://aur.archlinux.org/packages/librealsense-git).


## ONNX Runtime

As with Basalt just clone [onnxruntime-git](https://aur.archlinux.org/packages/onnxruntime-git) and build it.

`git clone https://aur.archlinux.org/basalt-monado-git.git && cd onnxruntime-git && makepkg -si`

Or use your favorite AUR Helper like [yay](https://aur.archlinux.org/yay.git). If you do not want to use the AUR or aren't using an Arch distribution, you can build it from source using [onnxruntime](https://github.com/microsoft/onnxruntime) or install the newest prebuild Release, although this is not recommended as they are often out of date.

## Monado

Same deal here, just make sure that you have all the other optional dependencies you want installed aswell, clone [monado-git](https://aur.archlinux.org/packages/monado-git) and build it.

`git clone https://aur.archlinux.org/monado-git.git && cd monado-git && makepkg -si`

Or use your favorite AUR Helper like [yay](https://aur.archlinux.org/yay.git). If you do not want to use the AUR or aren't using an Arch distribution, you can build it from source using [monado](https://monado.freedesktop.org/getting-started.html#installation-from-source).

Now, if you have build onnxruntime, run [get-ht-models.sh](https://gitlab.freedesktop.org/monado/monado/-/blob/main/scripts/get-ht-models.sh), otherwise you will get an error when starting `monado-service`.

## SteamVR

[SteamVR Driver instructions]({% link steamvr.md %}) are held here.

# Check it

Reminder that depending on your hardware, you will need to use `xrandr` to set the port its connected to as non-desktop.
`xrandr --output HDMI-A-0 --prop --set non-desktop 1` HDMI-A-0 being an example port.
Also make sure that you get camera output, these are basically webcams, so they need a lot of bandwith. If you get a single color image try a different USB 3 port and try removing usb devices from the corresponding USB controller.



## Monado

Run `monado-service`, if it gives an error about Wayland you can just ignore it as we are using X right now. The output should look pretty much like this:
<div style="text-align:center">
<img src="img/Monado-service.png"><br>
<sup>Image provided by VincentV<br>
</div>

## Basalt SLAM
Run `XRT_DEBUG_GUI=1 monado-service`, this will open a Debug Menu, check for "SLAM Tracker #1" and if it's not enabled already enable "Submit data to SLAM", otherwise you will not have any head tracking at all.
<div style="text-align:center">
<img src="img/SLAM_MonadoGUI.png"><br>
<sup>Image provided by VincentV<br>
</div>
Alternatively you can also run all programs/monado-service with "SLAM_SUBMIT_FROM_START=1" so you don't have to enable this manually if it is not already.

## ONNX Runtime
Run `XRT_DEBUG_GUI=1 monado-service`, this will open a Debug Menu, check for "Camera-based Hand Tracker #1" and check the Camera feeds, if you place your hands in the cameras FOV they should get colored dots on them and get tracked. You will need good lighting for this.

<div style="text-align:center">
<img src="img/Hand_Tracking_MonadoGUI.png"><br>
<sup>Image provided by VincentV<br>
</div>


# Current Issues
## General
Xrandr will refuse to work while monado-service is running if your monitors are rotated, this can be ignored though.

While using Hand tracking as controllers, you will notice sporadic inputs. (This will also improve greatly with the newer Hand tracking)
Under SteamVR, the Hand tracking controllers location will be incorrect.
The only way to pass skeletal input from Hand tracking controllers into SteamVR is with this environment variable:
`STEAMVR_EMULATE_INDEX_CONTROLLER=1`

If your HMD flies away from the playspace, or starts rapidly rotating, shut down SteamVR/Monado and replug your HMD.
Currently controllers for SLAM-based HMDs do not have 6DOF tracking. Only Nolo Controllers are mostly working.

SLAM tracking is only using 2 cameras at this point in time, but there is already great [progress](https://www.youtube.com/watch?v=IcAAYrcLxFQ&ab_channel=MateodeMayo), you can help with development [here](https://gitlab.freedesktop.org/mateosss/basalt/-/tree/mateosss/multicam).

## Rift S

Currently controllers will be used first, even if they are not connected.
You can force Hand tracking to be used as controllers instead with this environment variable:
`RIFT_S_HAND_TRACKING_AS_CONTROLLERS=1`



## Lenovo Explorer
There is no controller support at all yet.

## Useful Software

[Monado simple Playground](https://gitlab.freedesktop.org/monado/demos/openxr-simple-playground) This is just a small Demo "Game" to test Hand tracking.

[OpenComposite](https://gitlab.com/znixian/OpenOVR) is a program that forwards calls directly to the OpenXR runtime so you can skip SteamVR, it doesn't work with every game but it might give you better performance or better Hand tracking.

[LÖVR](https://lovr.org/)

[Stereokit](https://stereokit.net/)
