---
title: "Monado - GSoC proposal guide"
layout: main
---

# Google Summer of Code Application Guide
{:.no_toc}

* TOC
{:toc}


This page has some details on how you should prepare your proposal. It is only a guide so please do not feel constrained to follow it to the letter!

# Things to think about before applying
Before you consider applying to work on Monado for Google Summer of Code 2022, please consider the following points:

Students who are accepted into the Google Summer of Code program are expected to work full-time hours for the 12 weeks of the program -- that's 40 hours a week. It is not a good idea to try to do GSoC and also work another job; there simply isn't enough time. It's okay if your schedule doesn't allow 40 hours weeks every week (for instance, if you are going on vacation) but you'll have to let us know so that we can work around it.

Monado is a complex and very modular framework. Any GSoC project for Monado will almost certainly involve becoming closely familiar with the internal workings of the codebase. Although mentors exist to help out with this process, it's unreasonable to expect them to be able to explain every part of every paper you might need to read. So some level of self-motivation and definitely a willingness to learn are prerequisites.

Participation in the community (Discord or IRC) is highly encouraged and helps us get a better picture of who you are, your abilities, and how you will be able to contribute to the library over the summer. In addition, the community is there to help you out! Feel free to ask questions.

# Basic information
Once you've considered those three points, and want to write an application, make sure that your application has the following basic information. If you've already made yourself known to the community, make sure any aliases you've used there are posted here, so that we can know this application corresponds to someone we have already met.

* Name
* University / school
* Field of study
* Date study was started
* Expected graduation date
* Homepage
* Email
* IRC nick (if applicable)
* Interests and hobbies

Also, be sure to answer the following questions about your technical proficiency and coding skills:

* What languages do you know? Rate your experience level (1-5: rookie-guru) for each.
* How long have you been coding in those languages?
* Are you a contributor to other open-source projects?
* Do you have a link to any of your work (i.e. github profile)?
* What areas of XR (AR, VR) are you familiar with?
* Do you own some AR, VR gear already ?

# The project proposal
The most important part of a proposal is a project proposal. After including the information above, you should write a project proposal, which should clearly outline the following things:

* The objectives of the proposed project. This should detail what you expect to have accomplished at the end of the summer.
* Some background information on the project. This may include descriptions of the algorithms / data structures / ideas you plan to implement. Especially for more complex ideas, be sure to detail background information that a person who is reasonably familiar with Monado will be able to understand your description without needing to consult other references (but, when relevant, do link to other references).
* If applicable, describe the API of your finished project and how it will fit in with the rest of the Monado code. We realize that it's impossible to know exactly how the API will work when the project is not yet started, but you should at least have a basic idea of how you might want it to work.
* Describe how you will test your project.
* Describe a timeline of some sort for your project, broken down week by week (or thereabouts). In reality we understand that things don't often go according to plan, but having a predefined plan can significantly help the development process.

# Other information and tips for success
Please feel free to include any other information that you think is relevant but none of the above questions cover.

Being accepted into Google Summer of Code is a competitive process and the proposal process may be time-consuming. Prospective students who have already contributed code to the library and are participating in the community are more likely to be selected, as seeing contributions is helpful for displaying your talent and ability.

One good place to start might be the "Getting Started" page on the Monado website:

<https://monado.freedesktop.org/getting-started.html>

You can also look for ideas on the project ideas page:

<https://monado.freedesktop.org/gsoc-2022.html>

We look forward to hearing from you!
