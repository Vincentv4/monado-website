---
title: "Monado - Getting Started"
layout: main
---

* TOC
{:toc}

## About OpenXR and Monado

Software that supports VR by using the OpenXR API requires two software packages to work:

* The OpenXR Loader provided by Khronos
* An OpenXR runtime like Monado or SteamVR (SteamVR acts as a runtime for both OpenVR and OpenXR applications at the same time)

OpenXR runtimes like Monado can be though of as "VR headset and VR controller drivers" and the OpenXR loader, analog to the Vulkan loader, is responsible for finding and connecting applications to this "driver" so that OpenXR applications do not need to interact directly with a runtime.\\
Applications link to only the libopenxr_loader.so or .dll library and use the C headers provided by the OpenXR SDK.

More general background information about OpenXR and OpenXR runtimes can be found at [About Runtimes]({% link about-runtimes.md %}).

## Monado Requirements

Monado currently runs on linux and android. A windows port is in progress.

The Monado compositor requires a Vulkan driver with certain Vulkan extensions. For example the compositor requires VK_KHR_external_memory_fd and VK_KHR_external_semaphore_fd to enable the most basic submission of textures from the application to the compositor.

A full list of required and optional Vulkan extensions is available [in the doxygen documentation](https://monado.pages.freedesktop.org/monado/vulkan-extensions.html).

In particular reasonably modern versions of radv, intel anv and the nvidia proprietary driver are tested and confirmed to work.

OpenXR applications using OpenGL require an OpenGL driver with support for the `GL_EXT_memory_object_fd` OpenGL extension. OpenGL applications have been tested and are confirmed to work with radeonsi, intel i965 and iris (since mesa 21.2) and the nvidia proprietary driver.

Note that the intel drivers offer only limited support for GL_EXT_memory_object_fd: [depth/stencil formats are not supported yet](https://gitlab.freedesktop.org/mesa/mesa/-/merge_requests/10864).

Running Monado with the amdvlk Vulkan driver generally works but may not render OpenXR applications using OpenGL correctly.

## Monado Installation

There are no prebuilt generic binaries for monado available at this time.\\
If your distribution does not provide packages for the OpenXR SDK and Monado, you will have to build Monado from source.

### Distribution packages

Packages for the OpenXR SDK and Monado are available for various distributions.

* [Debian: releases in distro, Monado continuous builds in CI]({% link packages-debian.md %})
* [Ubuntu: releases in distro via Debian or through a PPA, Monado continuous builds in CI]({% link packages-ubuntu.md %})
* Archlinux [AUR: openxr-loader-git](https://aur.archlinux.org/packages/openxr-loader-git/) and [AUR: monado-git](https://aur.archlinux.org/packages/monado-git/)

Up to date information can be found [on repology](https://repology.org/project/monado/versions).

#### Debian and Ubuntu packages
{: #deb}
In Debian and Ubuntu the OpenXR SDK is split into several packages.

When using the precompiled debian packages, these packages are required to build and run OpenXR applications with Monado:

    apt install libopenxr-loader1 libopenxr-dev libopenxr1-monado

* libopenxr-loader1: This package contains the libopenxr_loader.so library. OpenXR applications link to this library and can not be compiled or run without it.
* libopenxr-dev: The OpenXR headers are required to compile OpenXR C/C++ applications.
* libopenxr1-monado: The Monado OpenXR runtime

Other useful packages:

* xr-hardware: udev rules allowing users without root permissions to use XR hardware, installation is highly recommended
* libopenxr-utils: OpenXR applications and demos, including the `hello_xr` example
* openxr-layer-corevalidation: A validation layer for OpenXR. Can be enabled with the environment variable `XR_ENABLE_API_LAYERS=XR_APILAYER_LUNARG_core_validation`
* openxr-layer-apidump: Another layer that can dump all OpenXR calls an application makes to a file
* monado-cli, monado-gui: See [Tools](#tools) for a detailed description

### Installation from Source

Install the CMake and ninja build tools

    apt install cmake ninja-build

See [https://gitlab.freedesktop.org/monado/monado#getting-started](https://gitlab.freedesktop.org/monado/monado#getting-started) for a list of dependencies.

This command will install required and some optional dependencies that will enable most of the commonly used functionality of monado on Debian/Ubuntu.
Some features and drivers are only compiled with additional dependencies like libsurvive or librealsense.

    apt install build-essential git wget unzip cmake ninja-build libeigen3-dev curl patch python3 pkg-config libx11-dev libx11-xcb-dev libxxf86vm-dev libxrandr-dev libxcb-randr0-dev libvulkan-dev glslang-tools libglvnd-dev libgl1-mesa-dev ca-certificates libusb-1.0-0-dev libudev-dev libhidapi-dev libwayland-dev libuvc-dev libavcodec-dev libopencv-dev libv4l-dev libcjson-dev libsdl2-dev libegl1-mesa-dev

then compile and install Monado. Monado can be built with either cmake or meson.

    git clone https://gitlab.freedesktop.org/monado/monado.git
    cmake -G Ninja -S monado -B build -DCMAKE_INSTALL_PREFIX=/usr
    ninja -C build install

### Monado Service

Since version 0.2, Monado can be built in two different modes: With `monado-service` (this is the default) and without `monado-service`.

The service can be disabled with `cmake -DXRT_FEATURE_SERVICE=OFF` or `meson -Dservice=false`.

#### With `monado-service`
{: #service}

When monado is built with `monado-service`, Monado's compositor and drivers run in a separate service process that has to be started before running an OpenXR application. `monado-service` will be installed as `/usr/bin/monado-service` in a default installation.

`monado-service` can either be started manually by running the binary, or it can be run automatically by using systemd socket activation.

Most monado developers and users who want exact control over when Monado is running are expected to run `monado-service` manually.

A manually started `monado-service` is cleanly shut down by simply pressing enter in the terminal it was started in.

An unclean shutdown (ctrl+c, crash) will leave a socket file `/run/user/1007/monado_comp_ipc`. If Monado's build configuration includes the `libbsd` library, this unused socket file will be automatically cleaned up by the next monado run. If Monado was built without support for `libbsd`, the Monado compositor will refuse to start if this file is found an error message. It safe to simply delete this file, as long as `monado-service` is not running - it holds no information, it is only used for live communication between monado-service and OpenXR applications.


If systemd is available (and it's not configured to disable this), a monado.socket and monado.service user unit files are installed in /usr/lib/systemd/user or similar. `systemctl --user enable monado.socket` will have systemd open the domain socket at login. Running an OpenXR application will spin up the service, while `systemctl --user stop monado.service` will stop it. This is expected to mainly be used by end users installing a package.

#### Without `monado-service`

When monado is built with the service disabled, the `monado-service` binary is not built. Instead of connecting to a long running service instance, OpenXR applications load the entire monado runtime as a library, initialize it at startup and shut it down on exit.

This mode is very convenient for debugging the Monado runtime, but makes it impossible to run overlay applications with `XR_EXTX_overlay`.


## Running OpenXR Applications

### OpenXR SDK

Monado's own tools run without the OpenXR loader, but running OpenXR applications requires the OpenXR loader provided by Khronos in the OpenXR SDK. Compiling OpenXR applications requires the OpenXR headers from the OpenXR SDK.

#### OpenXR SDK distribution packages

The OpenXR SDK is packaged in various distributions using different naming conventions. Check for availability of packages with names like [openxr](https://repology.org/project/openxr/versions), [openxr-loader](https://repology.org/project/openxr-loader/versions), [openxr-sdk](https://repology.org/project/openxr-sdk/versions) or [openxr-sdk-source](https://repology.org/project/openxr-sdk-source/versions).

OpenXR packages for various Ubuntu versions are also available [in the Monado PPA](https://launchpad.net/~monado-xr/+archive/ubuntu/monado).

#### Building the OpenXR SDK from source

If no package is available for your distribution or you want to use an updated version of the loader, you will have to compile the OpenXR loader from source. There are two repositories for the OpenXR SDK available.

* [OpenXR-SDK-Source](https://github.com/KhronosGroup/OpenXR-SDK-Source) generates the entire SDK from sources like the `xr.xml` API registry and contains additional examples like hello_xr. Therefore it requires quite a lot of dependencies.
* [OpenXR-SDK](https://github.com/KhronosGroup/OpenXR-SDK) is the simplified version of the SDK. It contains already generated headers and source code for the loader only and has only few dependencies.

See [https://github.com/KhronosGroup/OpenXR-SDK#linux](https://github.com/KhronosGroup/OpenXR-SDK#linux) for a list of dependencies.

Build the OpenXR SDK with cmake, this example uses ninja.

    git clone https://github.com/KhronosGroup/OpenXR-SDK.git
    cd OpenXR-SDK
    cmake . -G Ninja -DCMAKE_INSTALL_PREFIX=/usr -Bbuild
    ninja -C build install

### Selecting the Monado runtime for OpenXR applications

The OpenXR loader chooses the OpenXR runtime to load by first looking at the environment variable `XR_RUNTIME_JSON`. If this variable is not set, a file called `active_runtime.json` is searched in various locations.

Most installations of Monado will ship an `active_runtime.json` symlink in a systemwide xdg config path, which will make the OpenXR loader use Monado when starting OpenXR applications as described [in the loader documentation](https://github.com/KhronosGroup/OpenXR-SDK-Source/blob/master/specification/loader/runtime.adoc). If the packager decided not to ship an `active_runtime.json` symlink, you can create it yourself:

    sudo mkdir -p /etc/xdg/openxr/1/
    sudo ln -s /usr/share/openxr/1/openxr_monado.json /etc/xdg/openxr/1/active_runtime.json

Alternatively the OpenXR Loader also knows user specific xdg config paths. Note that this will only be obeyed by applications that are **not** run as root.

    mkdir -p ~/.config/openxr/1
    ln -s /usr/share/openxr/1/openxr_monado.json ~/.config/openxr/1/active_runtime.json

The environment variable `XR_RUNTIME_JSON` can be used in absense of, or to override an active_runtime.json:

    XR_RUNTIME_JSON=/usr/share/openxr/1/openxr_monado.json ./application

### Driver Selection

Monado automatically initializes drivers in the order they appear in [target_lists.c](https://gitlab.freedesktop.org/monado/monado/-/blob/master/src/xrt/targets/common/target_lists.c).

For example for a connected HTC Vive, Monado will first attempt to use the "survive" driver, then the "vive" driver, and at last the "ohmd" (OpenHMD) driver. In this example, to use the "ohmd" driver, the "survive" and "vive" drivers should be disabled.

## Tools
{: #tools}

Monado comes with a number of tools.

### monado-service

See [Running OpenXR applications with `monado-service`](#service)

### monado-gui

`monado-gui` serves the important function of calibrating cameras for positional tracking. See [Setting up Playstation Move Controllers]({% link positional-tracking-psmove.md %}) for an example of using it for calibration.

`monado-gui` stores persistent configuration in `$XDG_CONFIG_HOME`, or by default `~/.config/monado/`. A configuration file is only required for setups that require camera calibration (PSVR, PS Move).

### monado-cli

`monado-cli probe` and the more verbose `monado-cli test` provide an easy way to test which supported devices monado finds and can open.

### monado-ctl

Running concurrent non-overlay OpenXR applications will result in Monado only presenting the first started application. monado-ctl allows choosing the currently presented application. This command is only installed when the monado service is enabled at build time.

`monado-ctl` lists all applications connected to the current monado service instance and their status.

`monado-ctl -p 1` switches monado to present the application with id `1` in the list reported by `monado-ctl`.

## Environment Variables

Note: When compiling monado without monado-service, the environment variables that apply to the service can be used with the OpenXR application directly.

Either `TRUE`/`FALSE` or `1`/`0` can be used to set boolean variables.

### General

Available for monado-service and OpenXR applications:

* `XRT_PRINT_OPTIONS=1`:  Print supported environment variables and current value during runtime

Available for OpenXR applications:

* `OXR_DEBUG_ENTRYPOINTS=1`: Print all OpenXR "entry point" function calls that the application performs. Note that `XR_ENABLE_API_LAYERS=XR_APILAYER_LUNARG_api_dump` performs a similar role and prints even more information.
* `OXR_TRACKING_ORIGIN_OFFSET_X=0.5`, `OXR_TRACKING_ORIGIN_OFFSET_Y=1.0`, `OXR_TRACKING_ORIGIN_OFFSET_Z=0.1`: The offset of the current tracking origin to the desired tracking origin. Moves everything (HMD, left and right controllers) in the opposite direction of this offset, e.g. a device calibrated to (0,0,0) on a table 1 meter above the floor with `OXR_TRACKING_ORIGIN_OFFSET_Y=1.0` sets the tracking origin to the floor, and the device's resulting position will be (0,1,0).

Available for monado-service:

* `PROBER_LOG=debug`: Print information about devices that are found and opened.
* `IPC_EXIT_ON_DISCONNECT=1`: Exit the service whenever a client quits.

### Compositor

Available for monado-service:

* `XRT_COMPOSITOR_LOG=debug`: Print compositor related debug information (selected GPU, driver, display mode, ...). See [Logging](#loglevel).
* `XRT_COMPOSITOR_PRINT_MODES=1`: Like `XRT_COMPOSITOR_LOG=debug`, but only prints mode. Useful for `XRT_COMPOSITOR_DESIRED_MODE`.
* `XRT_COMPOSITOR_DESIRED_MODE=2`: Choose another mode than the default (highest resolution, then highest refresh rate). See `XRT_COMPOSITOR_PRINT_MODES` for a list of modes supported by the HMD.
* `XRT_COMPOSITOR_FORCE_GPU_INDEX=3`: Choose the GPU to run the compositor on. See `XRT_COMPOSITOR_LOG=debug` for the index of available Vulkan devices.
* `XRT_COMPOSITOR_FORCE_CLIENT_GPU_INDEX=4`: Choose the GPU recommended to OpenXR client to use. Defaults to the GPU the compositor runs on. Expect breakage when choosing a different GPU.
* `XRT_COMPOSITOR_FORCE_WAYLAND=1`: Run the Monado compositor on wayland. Usually this should not be necessary.
* `XRT_COMPOSITOR_FORCE_XCB=1`: Do not use direct mode and run the Monado compositor in a window.
* `XRT_COMPOSITOR_XCB_FULLSCREEN=1`: Only when using `XRT_COMPOSITOR_FORCE_XCB`: Start the compositor as a fullscreen window.
* `XRT_COMPOSITOR_FORCE_NVIDIA=1`, `XRT_COMPOSITOR_FORCE_RANDR=1`: Force the use of nvidia or mesa/amdvlk flavor of direct mode initialization. Usually this should not be necessary.
* `XRT_COMPOSITOR_FORCE_NVIDIA_DISPLAY=PNP`: Allows using direct mode on nvidia GPUs with a GPU not listed in [`NV_DIRECT_WHITELIST` in comp_settings.h](https://gitlab.freedesktop.org/monado/monado/-/blob/master/src/xrt/compositor/main/comp_settings.h). See `XRT_COMPOSITOR_LOG=debug` for finding viable display strings.
* `XRT_COMPOSITOR_SCALE_PERCENTAGE=100`: Scale the render target size recommended to applications to render at. Affects all applications connecting to this service. Defaults to 140% supersampling.
* `XRT_MESH_SIZE=64`: Control the resolution of the meshes generated by distortion mesh computation (vive, psvr, openhmd, northstar).

Available for OpenXR applications

* `OXR_VIEWPORT_SCALE_PERCENTAGE=100`: Scale the render target size recommended to this application to render at. Affects only this application. Defaults to 100%. Applied on top of `XRT_COMPOSITOR_SCALE_PERCENTAGE`.
* `OXR_DEBUG_IPD_MM=58`: Change the default IPD (63mm) for drivers that do not support dynamic IPD adjustment.

### Logging
{: #loglevel}

Various components and drivers in Monado use a logging system with well known logging levels `trace`, `debug`, `info` `warn`, `error`.

At this time the log level can only be changed for components individually by using environment variables.
The default log level is `warn`.
Environment variables that make use of the log level system typically end with `_LOG`. Examples:

* `VIVE_LOG=debug`
* `XRT_COMPOSITOR_LOG=debug`
* `XRT_LOG=debug`: Log messages from various systems around monado not hooked up to a more specific category.

### Debug GUI

Available for OpenXR applications and monado-service:

* `OXR_DEBUG_GUI=1`: Open a separate debug window for observing and manipulating certain state. For example changing a tracking origin virtually moves devices tracked on that tracking origin.
  * the debug information shown for monado-service and OpenXR clients is different, for example monado-service shows a basic compositor frametiming graph.

### SteamVR plugin

These variables should be set for SteamVR, for example when starting from command line: `STEAMVR_EMULATE_INDEX_CONTROLLER=1 ~/.local/share/Steam/steamapps/common/SteamVR/bin/vrstartup.sh`
or in the Steam library properties for SteamvR: `STEAMVR_EMULATE_INDEX_CONTROLLER=1 %command%`

* `STEAMVR_EMULATE_INDEX_CONTROLLER=1`: Use SteamVR's Index controller profile with hardcoded bindings instead of monado's generated input profiles. May be useful to make legacy games work.
* `XRT_COMPOSITOR_SCALE_PERCENTAGE=100`: Scales SteamVR's render target size, like for the Monado compositor.

Instructions for setting up the SteamVR plugin can be found [here]({% link steamvr.md %})

## Developing with Monado

You can now start [developing with Monado]({% link developing-with-monado.md %})
