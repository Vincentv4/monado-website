---
title: "Monado - Developer Site"
layout: main
---

# Monado - XR Runtime (XRT)
{:.no_toc}

* TOC
{:toc}

## What is Monado?

Monado is an open source XR runtime delivering immersive experiences such as VR
and AR on mobile, PC/desktop, and other devices. Monado aims to be a complete
and conformant implementation of the OpenXR API made by Khronos. The project
is currently being developed for GNU/Linux and aims to support other operating
systems such as Windows in the near future.

## Important links

* Source code: <https://gitlab.freedesktop.org/monado/monado>
* GSoC 2022: [Proposal guide](https://monado.freedesktop.org/gsoc-proposal-guide.html)
  and [project ideas](https://monado.freedesktop.org/gsoc-2022.html).
* Discord server: <https://discord.gg/8RkJgRJ>
* IRC channel: [#monado](https://webchat.oftc.net/?channels=monado) on
  [OFTC](https://oftc.net/)

## Google Summer of Code 2022

We are happy to announce that Monado has been selected for
[Google Summer of Code 2022](https://summerofcode.withgoogle.com/).
A list of ideas for potential projects and additional information are available
on the Monado [proposal guide](https://monado.freedesktop.org/gsoc-proposal-guide.html)
and [project ideas](https://monado.freedesktop.org/gsoc-2022.html) page.

## Highlights

* [Officially an OpenXR conformant implementation](https://www.collabora.com/news-and-blog/news-and-events/monado-2100-officially-conformant-openxr-implementation.html) of the [OpenXR API](https://www.khronos.org/openxr)
  * Supports Vulkan, OpenGL and Headless applications
  * Full support for Local, Stage and Action space relations
  * Action based input
  * OpenXR Extensions:
    * XR_KHR_convert_timespec_time
    * XR_KHR_opengl_enable, XR_KHR_opengl_es_enable, XR_KHR_vulkan_enable, XR_KHR_vulkan_enable2
    * XR_KHR_composition_layer_depth, XR_KHR_composition_layer_cylinder, XR_KHR_composition_layer_equirect, XR_KHR_composition_layer_equirect2
    * XR_EXT_hand_tracking - with Index controllers on vive and libsurvive drivers and Leap Motion with Ultraleap v2
    * XR_EXTX_overlay - [Multi-application support](https://www.collabora.com/news-and-blog/news-and-events/monado-multi-application-support-with-xr-extx-overlay.html)
    * XR_MND_headless - XrSession without graphical output and compositor
    * XR_KHR_swapchain_usage_input_attachment_bit / XR_MND_swapchain_usage_input_attachment_bit
    * XR_MNDX_egl_enable - Alternative to XR_KHR_opengl_enable and XR_KHR_opengl_es_enable on Desktop
    * XR_KHR_android_create_instance, XR_KHR_loader_init and XR_KHR_loader_init_android - android specific
* 6DoF tracking, initial implementation for PSVR and PS Move controllers
  * Outside-in tracking framework with stereo cameras such as PS4 camera
  * Work in progress on outside-in tracking with mono cameras such as consumer webcams
* Video stream and filter framework for tracking components
* [Initial Android support](https://www.collabora.com/news-and-blog/news-and-events/monado-update-passing-conformance-android-support-and-more.html)
  * Cardboard viewer
* XR Compositor
  * [Direct mode on AMD, NVidia and Intel GPUs]({% link direct-mode.md %})
  * Mesh based distortion with generators for Panotools and Vive/Index parameters
  * Supports multiple simultaneous projection layers and quad layers
* Driver framework allowing easy integration of existing drivers
  * Out of the box support for multiple XR devices with open source drivers

## Supported Hardware

These are the XR devices that are natively supported with open source drivers in Monado

| Device | Rotation | Position | Distortion Correction | Additional Notes |
|:------:|:--------:|:--------:|:---------------------:|:----------------:|
| OSVR HDK 1.x, 2.x | Yes | No | No | Requires workaround on AMD GPUs[^yuv_edid]. Firmware fix available[^hdk_edid_fix] |
| HTC Vive | Yes | LH 1.0: No | Yes | Supported by OpenHMD, Monado "vive", or Monado "survive" |
| HTC Vive Pro | Yes | LH 1.0: No, LH 2.0: Early WIP| Yes | Supported by OpenHMD, Monado "vive", or Monado "survive" |
| Valve Index | Yes | LH 1.0: No, LH 2.0: Early WIP | Yes | Supported by Monado "vive", or Monado "survive" |
| North Star | Yes, with Intel Realsense T265* | Yes, with Intel Realsense T265* | Yes, 3D and 2D/Polynomial | |
| Oculus Rift S | Yes | Experimental*, for headset | Imperfect | Controllers are rotationally tracked only |
| PSVR | Yes | [Yes, with PS4 camera or generic stereo camera]({% link positional-tracking-psmove.md %}) | Yes | distortion correction is WIP. Requires workaround on AMD GPUs[^yuv_edid]. |
| Playstation Move | Yes | [Yes, with PS4 or generic stereo camera]({% link positional-tracking-psmove.md %}) | - | rotational drift correction is WIP |
| Hydra Controller | Yes | Yes | - | |
| Daydream Controller | Yes | - | - | |
| DIY arduino controller | Yes | - | - | |
| Windows MR | Yes | Experimental* | Yes | HMD only, no controllers. Tested: HP Reverb, HP Reverb G2, Samsung Odyssey, Lenovo Explorer, others: *maybe*. |

\* _Experimental 6DoF tracking support with external SLAM/VIO systems is upstreamed but still being worked on. Tested with RealSense D455 and Samsung Odyssey+, Reverb G2, Oculus Rift S._

Monado also leverages the open source drivers developed by the OpenHMD community
for further HMD support.
See the [OpenHMD support matrix](http://www.openhmd.net/index.php/devices/)
for a list of devices supported through OpenHMD.

The Direct-Tek WVR2 / VR-Tek Windows VR Glasses with the 2560x1440 resolution
supported through OpenHMD requires a workaround on AMD GPUs[^yuv_edid].

Other 3rd party open source drivers Monado currently wraps are

| 3rd party driver | Device | Rotation | Position | Distortion Correction | Additional Notes |
|:----------------:|:------:|:--------:|:--------:|:---------------------:|:----------------:|
| [libsurvive](https://github.com/cntools/libsurvive) | HTC Vive | Yes | Yes | Yes | [survive]({% link libsurvive.md %}) driver must be enabled at build time |
| [libsurvive](https://github.com/cntools/libsurvive) | HTC Vive Pro | Yes | Yes | Yes | [survive]({% link libsurvive.md %}) driver must be enabled at build time |
| [libsurvive](https://github.com/cntools/libsurvive) | Valve Index | Yes | Yes | Yes | [survive]({% link libsurvive.md %}) driver must be enabled at build time |
| [librealsense](https://github.com/IntelRealSense/librealsense) | T265 realsense | Yes | Yes | - | proprietary on-device SLAM tracking |

Proprietary 3rd party drivers can also be integrated. Monado currently wraps these

| proprietary 3rd party driver | Device | Rotation | Position | Distortion Correction | Additional Notes |
|:----------------------------:|:------:|:--------:|:--------:|:---------------------:|:----------------:|
| Ultraleap v2 | Leap Motion Controller | Yes | Yes | - | |

### So what does that mean?

For end users it means Monado can be used to run OpenXR games and
applications like Blender on any of the supported hardware.

For developers it means you can start developing software for OpenXR
with the ability to debug and inspect the source code of the entire XR
software stack from your application to the HMD driver.

Monado transparently takes care of direct mode and distortion correction
without developers having to write a single line of X11 code.

## Getting Started with Monado

* [Getting Started installing and running applications with Monado]({% link getting-started.md %})
* [The OpenXR 1.0 specification](https://www.khronos.org/registry/OpenXR/specs/1.0/html/xrspec.html)
* [List of Open Source applications and examples]({% link openxr-resources.md %})
* [Tested Multi-GPU configurations (e.g. Optimus)]({% link multi-gpu.md %})
* [For hacking on Monado (e.g. implementing OpenXR extensions), take a look at the online doxygen documentation](https://monado.pages.freedesktop.org/monado/)
* [Use Monado's hardware drivers in SteamVR]({% link steamvr.md %})
* [Install SLAM and hand tracking]({% link hand-tracking.md %})

## Code of Conduct

We follow the standard freedesktop.org code of conduct, available at
<https://www.freedesktop.org/wiki/CodeOfConduct/>, which is based on the
[Contributor Covenant](https://www.contributor-covenant.org).

Instances of abusive, harassing, or otherwise unacceptable behavior may be
reported by contacting:

* First-line project contacts:
  * Jakob Bornecrantz <jakob@collabora.com>
  * Ryan Pavlik <ryan.pavlik@collabora.com>
* freedesktop.org contacts: see most recent list at
  <https://www.freedesktop.org/wiki/CodeOfConduct/>

## Contributing & Repos

The main repository is <https://gitlab.freedesktop.org/monado/monado> and has
documentation on how to get started and contribute. Please refer to the
`CONTRIBUTING.md` file in the repository for details.

Contributions to the information on this website itself are welcome at
<https://gitlab.freedesktop.org/monado/webpage>

## Contact

For other questions and just hanging out you can find us here:

* [Discord](https://discord.gg/8RkJgRJ) server.
* [#monado](https://webchat.oftc.net/?channels=monado) on [OFTC](https://oftc.net/).

# Footnotes
{:.no_toc}

[^yuv_edid]: An issue with the EDID results in wrong colors or black screen on AMD GPUS. See [EDID override]({% link edid-override.md %}) for details and workarounds.
[^hdk_edid_fix]: Firmware was fixed [in this PR](https://github.com/OSVR/OSVR-HDK-MCU-Firmware/pull/31). Builds can be found [here](https://dev.azure.com/osvr/OSVR-HDK-MCU-Firmware/_build/results?buildId=9&view=artifacts&type=publishedArtifacts) - Choose a successful pipeline (green checkmark), choose "1 published", in the 3 dots menu choose "Download Artifacts".
