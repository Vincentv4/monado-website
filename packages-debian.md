---
title: "Monado - Debian Packages"
layout: main
---

## Distribution

Monado is packaged in Debian Unstable (sid), uploaded typically when a new release is tagged. Barring any freezes or dependency issues, the packages automatically migrate to Testing (bullseye). At that time, we typically try to upload a package to buster-backports as well.

xr-hardware and the OpenXR loader are also in sid, bullseye, and buster-backports already.

All Debian and Ubuntu packages of Monado for Linux are built with service
support enabled: see
[these notes](https://gitlab.freedesktop.org/monado/monado/-/blob/debian/sid/debian/NEWS)
on how to enable, start, and stop Monado.

## CI builds for buster

If you like living dangerously, you can install packages directly from the latest master CI build with this additional repository:

```sh
# Download CI signing key
sudo wget -nv https://monado.freedesktop.org/keys/monado-ci.asc -O /etc/apt/trusted.gpg.d/monado-ci.asc

# Add apt repo
echo 'deb https://monado.pages.freedesktop.org/monado/apt buster main' | sudo tee /etc/apt/sources.list.d/monado-ci.list

# Update package lists
sudo apt update
```

For a description of the packages see [Getting Started]({% link
getting-started.md %}). Note that only the Monado packages are in this package
repo: you still will want to install the OpenXR loader and xr-hardware from
Debian (or backports).
